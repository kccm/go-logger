package logger

import (
	log "github.com/sirupsen/logrus"
	"os"
)

var (
	AppName = ""
)

type Fields map[string]interface{}

func init() {
	// Default value if not setup appName with Set()
	Set("undefined", false)
}

// Logger Setup
func Set(logName string, debug bool) {
	// Set Log Formatter to JSON formatter
	log.SetFormatter(&log.JSONFormatter{})

	//Get and Configure the name on Application Level
	if logName != "" {
		AppName = logName
	}

	// Set Logger Output
	log.SetOutput(os.Stdout)

	// Set Log Level -  Debugging is enable/disable
	if debug {
		log.SetLevel(log.DebugLevel) // Debug Level +
	} else {
		log.SetLevel(log.InfoLevel) // Warn Level +
	}
}

// ("Msg", {"key":value})
func Debug(message string, other ...map[string]interface{}) {
	fields := map[string]interface{}{}
	if other != nil {
		fields = other[0]
	}
	fields["[ ! ]"], fields["name"] = message, AppName
	log.WithFields(fields).Debug("[ ! ]")
}

// ("UserEmail", "Msg", {"key":value})
func Info(who string, message string, other ...map[string]interface{}) {
	fields := map[string]interface{}{}
	if other != nil {
		fields = other[0]
	}
	fields["by"], fields["name"] = who, AppName
	log.WithFields(fields).Info(message)
}

// ("UserEmail", error, "err", {"key":value})
func Fatal(who string, err error, message string, other ...map[string]interface{}) {
	fields := map[string]interface{}{}
	if other != nil {
		fields = other[0]
	}
	fields["by"], fields["name"] = who, AppName
	log.WithFields(fields).WithError(err).Fatal(message)
}

// ("UserEmail", error, "err", {"key":value})
func Error(who string, err error, message string, other ...map[string]interface{}) {
	fields := map[string]interface{}{}
	if other != nil {
		fields = other[0]
	}
	fields["by"], fields["name"] = who, AppName
	log.WithFields(fields).WithError(err).Error(message)
}
