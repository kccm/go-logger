## Logger 

# Setup
```
log.Set("Service-Name", true) // Service Name, debug mode
```

# Usage
```
// Debug Log
log.Debug("This is a debug log", log.Fields{"key": "value"}) // message, data (option: data)
// Info Log
log.Info("SYSTEM", "This is a info log", log.Fields{"key": "value"}) // creator, message, data (option: data)
// Error Log
log.Error("SYSTEM", errors.New("Error."), "This is a error log", log.Fields{"key": "value"}) // creator, error, message, data (option: data)
// Fatal Log
log.Fatal("SYSTEM", errors.New("Error."), "This is a fatal log", log.Fields{"key": "value"}) // creator, error, message, data (option: data)
```

# Log Standard Format
| **Field Name** | **Type** | **Description** |   
|---------------|----------|--------------------|
| msg | String | Required for all log |
| name | String | Service Name |
| by | String | "SYSTEM" or Email of log creator |