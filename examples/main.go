package main

import (
	"errors"
	log "gitlab.com/kccm/go-logger"
)

func main() {
	// Setup
	log.Set("Example Service", true) // Service Name, debug mode

	// Debug Log
	log.Debug("This is a debug log", log.Fields{"key": "value"}) // message, data (option: data)
	// Info Log
	log.Info("SYSTEM", "This is a info log", log.Fields{"key": "value"}) // creator, message, data (option: data)
	// Error Log
	log.Error("SYSTEM", errors.New("Error."), "This is a error log", log.Fields{"key": "value"}) // creator, error, message, data (option: data)
	// Fatal Log
	log.Fatal("SYSTEM", errors.New("Error."), "This is a fatal log", log.Fields{"key": "value"}) // creator, error, message, data (option: data)
}
